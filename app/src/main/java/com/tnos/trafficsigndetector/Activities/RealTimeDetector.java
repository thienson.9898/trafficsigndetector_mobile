package com.tnos.trafficsigndetector.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Guideline;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.RectF;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.tnos.trafficsigndetector.Activities.Adapter.ListTrafficSignAdapter;
import com.tnos.trafficsigndetector.Common.CameraPreview;
import com.tnos.trafficsigndetector.Common.CanvasView;
import com.tnos.trafficsigndetector.Common.Constants;
import com.tnos.trafficsigndetector.Common.Utils;
import com.tnos.trafficsigndetector.Entities.BBox;
import com.tnos.trafficsigndetector.Entities.ModuleResult;
import com.tnos.trafficsigndetector.Entities.TrafficSign;
import com.tnos.trafficsigndetector.MainActivity;
import com.tnos.trafficsigndetector.R;

import org.pytorch.Module;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static com.tnos.trafficsigndetector.Common.Utils.getResizedBitmap;

public class RealTimeDetector extends AppCompatActivity {
    CanvasView canvasView;
    static Camera camera;
    CameraPreview cp;
    FrameLayout cameraViewContainer;
    List<BBox> listBBox = new ArrayList<>();
    Bitmap selectedImage;
    Module model;
    Utils utils = new Utils();
    ListTrafficSignAdapter adapter;
    ListView ListViewTrafficSign;
    ArrayList<TrafficSign> lsTrafficSign;
    Socket socket;
    String type;
    TextView txtFps;
    TextView txtDebug;
    long startDetect, responseDetect;
    long timeExecute;
    ModuleResult result;
    int maxZoom;
    Camera.Parameters parameters;
    SeekBar seekBarZoom;
    String serverDetectResult;

    boolean isAudioPlaying;
    long startAudio, endAudio;
    ArrayList<String> arrSignName = new ArrayList<>();
    int indexArrSignName = 0;
    MediaPlayer mp;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_real_time_detector);

        InitViews();
        InitEvents();
        InitModel();
        if (Constants.MethodDetect.equals(Constants.MethodDetect_Mb2_Server) || Constants.MethodDetect.equals(Constants.MethodDetect_VGG_Server)) {
            type = Constants.MethodDetect.equals(Constants.MethodDetect_Mb2_Server) ? "mb2-ssd-lite" : "vgg16-ssd";
            ConnectSocket();
            SocketResponseHandler();
        } else {
            StartDetectLocal();
        }
    }

    private void StartDetectLocal() {
        threadLocal.run();
    }

    private void StartDetectServer() {
        threadRunOnServer.run();
    }

    private void InitModel() {
        String pathModel = Utils.assetFilePath(RealTimeDetector.this, "mb2.pth");
        final String moduleFileAbsoluteFilePath = new File(pathModel).getAbsolutePath();
        try {
            model = Module.load(moduleFileAbsoluteFilePath);
            //Toast.makeText(PhotoDetector.this, "Loaded", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(RealTimeDetector.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void InitViews() {
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        cameraViewContainer = (FrameLayout) this.findViewById(R.id.rt_frame_ctn);
        setCameraView();
        canvasView = new CanvasView(this);
        canvasView.fullScreen = true;
        canvasView.canvasWidth = (float) (canvasView.getWidth());
        canvasView.canvasHeight = (float) (canvasView.getHeight());
        cameraViewContainer.addView(canvasView);
        txtDebug = (TextView) this.findViewById(R.id.rt_txt_debug);
        txtFps = (TextView) this.findViewById(R.id.rt_txt_fps);
        if (Constants.Debug.equals("1")) {
            txtFps.setVisibility(View.INVISIBLE);
            txtDebug.setVisibility(View.VISIBLE);
        } else {
            txtDebug.setVisibility(View.INVISIBLE);
            txtFps.setVisibility(View.VISIBLE);
        }

        lsTrafficSign = new ArrayList<>();
        ListViewTrafficSign = (ListView) this.findViewById(R.id.rt_list_view);
//        adapter = new ListTrafficSignAdapter(PhotoDetector.this, Constants.listTrafficSign, 1);
        adapter = new ListTrafficSignAdapter(RealTimeDetector.this, lsTrafficSign, 1);
        ListViewTrafficSign.setAdapter(adapter);
        parameters = camera.getParameters();
        if (parameters.getSupportedFocusModes().contains(
                Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        }
        camera.setParameters(parameters);
        seekBarZoom = (SeekBar) this.findViewById(R.id.rt_seek_bar_zoom);
//        seekBarZoom.setProgressTintList(ColorStateList.valueOf(Color.WHITE));
        maxZoom = parameters.getMaxZoom();
        Log.d("Seekbar", "MaxZoom: " + maxZoom);
        mp = new MediaPlayer();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Toast.makeText(RealTimeDetector.this, "XOAY XOAY", Toast.LENGTH_LONG).show();
    }

    private void InitEvents() {
        cameraViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(runnableHideSeekBar);
                if (seekBarZoom.getVisibility() == View.VISIBLE) {
                    seekBarZoom.setVisibility(View.INVISIBLE);
                } else if (seekBarZoom.getVisibility() == View.INVISIBLE) {
                    seekBarZoom.setVisibility(View.VISIBLE);
                    handler.postDelayed(runnableHideSeekBar, 2000);
                }
            }
        });
        seekBarZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("Seekbar", progress + "");
                if (parameters.isZoomSupported()) {
                    parameters.setZoom(maxZoom * progress / 100);
                    camera.setParameters(parameters);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                handler.removeCallbacks(runnableHideSeekBar);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                handler.postDelayed(runnableHideSeekBar, 2000);
            }
        });

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCompletion(MediaPlayer mp) {
                indexArrSignName += 1;
                endAudio = System.currentTimeMillis();
                if (endAudio - startAudio > Constants.timeDiff) {
                    arrSignName.clear();
                    isAudioPlaying = false;
                    return;
                }
                if (indexArrSignName >= arrSignName.size()) {
                    Log.d("Audio", "Set isAudioPlaying = false");
                    isAudioPlaying = false;
                    return;
                } else {
                    isAudioPlaying = true;
                }
                try {
                    mp.reset();
                    Log.d("audio", "Repeat");
                    mp.setDataSource(new Utils().GetLinkAudio(RealTimeDetector.this, arrSignName.get(indexArrSignName)));
                    mp.prepare();
                    mp.start();
                } catch (Exception ex) {
                    Log.d("audio", "Init Events, " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
        });

    }

    Runnable runnableHideSeekBar = new Runnable() {
        @Override
        public void run() {
            seekBarZoom.setVisibility(View.INVISIBLE);
        }
    };

    void setCameraView() {
        if (checkCameraHardware(this)) {
            Camera c = getCameraInstance();
            int orientation = this.getResources().getConfiguration().orientation;
            cp = new CameraPreview(this, c, orientation);
            cameraViewContainer.addView(cp);
        } else {
            Toast.makeText(this, "Không hỗ trợ camera", Toast.LENGTH_LONG);
        }
    }

    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public static Camera getCameraInstance() {
        camera = null;
        try {
            camera = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return camera; // returns null if camera is unavailable
    }

    //Init Thread cho việc detect
    Handler handler = new Handler();

    Thread threadLocal = new Thread(new Runnable() {
        @Override
        public void run() {
            handler.post(runnableDetectLocal);
        }
    });

    Runnable runnableDetectLocal = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            startDetect = System.currentTimeMillis();
            selectedImage = Utils.getBitmapFromView(cp);
            result = utils.RunModel(model, selectedImage, 0.5f);
            responseDetect = System.currentTimeMillis();
            timeExecute = responseDetect - startDetect;
            handler.post(runnableDrawCanvasLocal);
            if (!Constants.Sign_Audio.equals(Constants.Sign_Audio_None)) {
                AddSignNameToArr();
            }
            handler.post(this);
        }
    };

    Runnable runnableDrawCanvasLocal = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            canvasView.listBBox = result.listBBox;
            canvasView.invalidate();
            float fps = (1000f / (float) timeExecute);
            txtDebug.setText(String.format("Time: %.0fms\nFPS: %.5f\n-------------\n%s", (float) timeExecute, fps, result.result));
            txtFps.setText(String.format("Time: %.0fms\nFPS: %.5f", (float) timeExecute, fps));

            lsTrafficSign = utils.GetListTrafficSignForListView(result.listBBox);
            lsTrafficSign.remove(null);
            handler.post(this);
            if (lsTrafficSign != null) {
                if (lsTrafficSign.isEmpty()) {
                    lsTrafficSign.add(new TrafficSign("Không tìm thấy biển báo", "info_alert", "", "Không tìm thấy biển báo trong hình", ""));
                }
                adapter.lsTrafficSign = lsTrafficSign;
                adapter.notifyDataSetChanged();
            }
        }
    };

    Thread threadRunOnServer = new Thread(new Runnable() {
        @Override
        public void run() {
            handler.post(runnableOnServer);
        }
    });

    Runnable runnableOnServer = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            selectedImage = Utils.getBitmapFromView(cp);
            SocketDetector(type, selectedImage);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    void SocketDetector(String method, Bitmap selectedImage) {
        startDetect = System.currentTimeMillis();
        Bitmap bmp = getResizedBitmap(selectedImage, 300, 300);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        byte[] byteImg = stream.toByteArray();
        socket.emit(method, byteImg, Constants.ThresHold);
    }

    void ConnectSocket() {
        if (socket != null && socket.connected()) {
            socket.disconnect();
        }
        try {
            socket = IO.socket(Constants.ServerUrl);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.connect();
        socket.emit("ping");

    }

    void SocketResponseHandler() {
        socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.lsTrafficSign.clear();
                        lsTrafficSign.add(new TrafficSign("Không thể kết nối đến Server", "alert_icon", "", "Vui lòng kiểm tra lại kết nối Internet", ""));
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.lsTrafficSign.clear();
                        lsTrafficSign.add(new TrafficSign("Không thể kết nối đến Server", "alert_icon", "", "Vui lòng kiểm tra lại kết nối Internet", ""));
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StartDetectServer();
                    }
                });
            }
        });

        socket.on("response", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void run() {
                        handler.post(runnableOnServer);
                        responseDetect = System.currentTimeMillis();
                        long timeExecute = responseDetect - startDetect;
                        float timePredict = Float.valueOf(args[0].toString().split("\r\n")[0].split(":")[1]) * 1000f;
                        float timeDelay = (float) timeExecute - timePredict;
                        serverDetectResult = (String) args[0];
                        handler.post(runnableDrawCanvasServer);
                        if (!Constants.Sign_Audio.equals(Constants.Sign_Audio_None)) {
                            AddSignNameToArr();
                        }
                        float fps = (1000f / timeExecute);
                        txtDebug.setText(String.format("TimePredict: %.0fms\nTimeExecute: %.0fms\nFPS: %.5f\nDelay: %.0fms\n-------------\n%s", timePredict, (float) timeExecute, fps, timeDelay, serverDetectResult));
                        txtFps.setText(String.format("TimePredict: %.0fms\nTimeExecute: %.0fms\nFPS: %.5f\nDelay: %.0fms", timePredict, (float) timeExecute, fps, timeDelay));
                    }
                });
            }
        });
    }

    Runnable runnableDrawCanvasServer = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            listBBox.clear();
            String[] rs = serverDetectResult.split("\r\n");
            if (rs.length >= 2) {
                for (int i = 1; i < rs.length; i++) {
                    String rectCo = rs[i].split(":")[2];
                    String[] item = rectCo.split(",");
                    float xmin = (float) (Float.valueOf(item[0]) / 300f);
                    float ymin = (float) (Float.valueOf(item[1]) / 300f);
                    float xmax = (float) (Float.valueOf(item[2]) / 300f);
                    float ymax = (float) (Float.valueOf(item[3]) / 300f);
                    RectF rect = new RectF(xmin, ymin, xmax, ymax);
                    listBBox.add(new BBox(rs[i].split(":")[0], Float.valueOf(rs[i].split(":")[1]), rect));
                }
            }
            listBBox = Utils.NMSx00(listBBox, 0.5f);
            canvasView.listBBox = listBBox;
            canvasView.invalidate();
            lsTrafficSign = utils.GetListTrafficSignForListView(listBBox);
            lsTrafficSign.remove(null);
            if (lsTrafficSign != null) {
                if (lsTrafficSign.isEmpty()) {
                    lsTrafficSign.add(new TrafficSign("Không tìm thấy biển báo", "info_alert", "", "Không tìm thấy biển báo trong hình", ""));
                }
                adapter.lsTrafficSign = lsTrafficSign;
                adapter.notifyDataSetChanged();
            }
        }
    };


    void AddSignNameToArr() {
        boolean added = false;
        endAudio = System.currentTimeMillis();
        if (endAudio - startAudio > Constants.timeDiff && isAudioPlaying == false) {
            arrSignName.clear();
//                isAudioPlaying = false;
        }
        for (TrafficSign sign : lsTrafficSign) {
            if (sign != null && sign.name != null) {
                if (sign.name.contains("Không tìm thấy biển báo")) {
                    continue;
                }
            }
            //drawableSource cũng là file audio
            if (!arrSignName.contains(sign.drawableSource)) {
                arrSignName.add(sign.drawableSource);
                added = true;
                //Log.d("Audio", "Thêm biển " + sign.name + " vào list");
            }
        }
        if (added && isAudioPlaying == false) {
//            Log.d("Audio","Post Runnable Audio");
            indexArrSignName = 0;
            handler.post(runnableAudio);
        }
    }

    //Audio thread;;;;;
    Runnable runnableAudio = new Runnable() {
        @Override
        public void run() {
            startAudio = System.currentTimeMillis();
            try {
                mp.reset();
                mp.setDataSource(Utils.GetLinkAudio(RealTimeDetector.this, arrSignName.get(indexArrSignName)));
                mp.prepare();
                mp.start();
            } catch (Exception ex) {
            }

        }
    };

    Runnable runnableClearAudio = new Runnable() {
        @Override
        public void run() {
            indexArrSignName = 0;
            arrSignName.clear();
            Log.d("Audio", "Index = 0,, Clear list after 5s");
            handler.removeCallbacks(this);
        }
    };

    @Override
    public void onBackPressed() {
        if (parameters.isZoomSupported() && parameters.getZoom() != 0) {
            parameters.setZoom(0);
            camera.setParameters(parameters);
            seekBarZoom.setProgress(0);
            return;
        }
        super.onBackPressed();
        if (socket != null && socket.connected()) {
            socket.off();
            socket.disconnect();
        }
        try{
            mp.reset();
        }
        catch(Exception e){

        }
        adapter.lsTrafficSign.clear();
        adapter.notifyDataSetChanged();
        canvasView.listBBox.clear();
        canvasView.invalidate();
        threadLocal.interrupt();
        threadRunOnServer.interrupt();
        handler.removeCallbacksAndMessages(null);
        this.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (parameters.isZoomSupported() && parameters.getZoom() != 0) {
            parameters.setZoom(0);
            camera.setParameters(parameters);
            seekBarZoom.setProgress(0);
            return;
        }
        onBackPressed();
    }
}