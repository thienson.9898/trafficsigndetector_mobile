package com.tnos.trafficsigndetector.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.tnos.trafficsigndetector.Activities.Adapter.ListTrafficSignAdapter;
import com.tnos.trafficsigndetector.Common.CanvasView;
import com.tnos.trafficsigndetector.Common.Constants;
import com.tnos.trafficsigndetector.Common.Utils;
import com.tnos.trafficsigndetector.Entities.BBox;
import com.tnos.trafficsigndetector.Entities.ModuleResult;
import com.tnos.trafficsigndetector.Entities.TrafficSign;
import com.tnos.trafficsigndetector.R;

import org.pytorch.Module;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import static com.tnos.trafficsigndetector.Common.Utils.getResizedBitmap;

public class PhotoDetector extends AppCompatActivity {
    ImageButton btnChooseImg;
    ImageView imageView;
    ListView listViewTrafficSign;
    CanvasView canvasView;
    List<BBox> listBBox = new ArrayList<>();
    Bitmap selectedImage;
    Module model;
    FrameLayout container;
    Utils utils = new Utils();
    ListTrafficSignAdapter adapter;
    ArrayList<TrafficSign> lsTrafficSign;
    Guideline line;
    Socket socket;
    String type;

    final int REQUEST_CODE_GALLERY = 1;
    final int REQUEST_CODE_CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detector);
        InitView();
        InitEvent();
        InitModel();
        //Trap a socket
        if (Constants.MethodDetect.equals(Constants.MethodDetect_Mb2_Server) || Constants.MethodDetect.equals(Constants.MethodDetect_VGG_Server)) {
            type = Constants.MethodDetect.equals(Constants.MethodDetect_Mb2_Server) ? "mb2-ssd-lite" : "vgg16-ssd";
            ConnectSocket();
            SocketResponseHandler();
        } else {
            adapter.lsTrafficSign.clear();
            lsTrafficSign.add(new TrafficSign("Vui lòng chọn hình ảnh", "success_icon", "", "Vui lòng chọn hình ảnh có chứa biển báo!", ""));
            adapter.notifyDataSetChanged();
        }
    }

    private void InitModel() {
        String pathModel = Utils.assetFilePath(PhotoDetector.this, "mb2.pth");
        final String moduleFileAbsoluteFilePath = new File(pathModel).getAbsolutePath();
        try {
            model = Module.load(moduleFileAbsoluteFilePath);
            //Toast.makeText(PhotoDetector.this, "Loaded", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(PhotoDetector.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void InitEvent() {
        btnChooseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
            }
        });
    }

    private void InitView() {
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        btnChooseImg = (ImageButton) this.findViewById(R.id.pd_btn_choose_img);
        imageView = (ImageView) this.findViewById(R.id.pd_img_view);
        listViewTrafficSign = (ListView) this.findViewById(R.id.pd_list_view_traffic_sign);
        container = (FrameLayout) this.findViewById(R.id.pd_frame_img_ctn);
        canvasView = new CanvasView(this);
        container.addView(canvasView);
        lsTrafficSign = new ArrayList<>();
//        adapter = new ListTrafficSignAdapter(PhotoDetector.this, Constants.listTrafficSign, 1);
        adapter = new ListTrafficSignAdapter(PhotoDetector.this, lsTrafficSign, 1);
        listViewTrafficSign.setAdapter(adapter);
        line = (Guideline) this.findViewById(R.id.pd_guideline);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap Image = null;
            if (reqCode == REQUEST_CODE_CAMERA) {
                File imgFile = new  File(currentPhotoPath);

                if(imgFile.exists()){
                    Image = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Image = Bitmap.createBitmap(Image, 0, 0, Image.getWidth(), Image.getHeight(), matrix, true);

                }

            }
            if (reqCode == REQUEST_CODE_GALLERY) {
                final Uri imageUri = data.getData();
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(imageUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Image = BitmapFactory.decodeStream(imageStream);
            }

            try {
                adapter.lsTrafficSign.clear();
                adapter.notifyDataSetChanged();

                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                canvasView.listBBox.clear();
                canvasView.invalidate();

                SetLocationGuideLine(Image);
                imageView.setImageBitmap(Image);
                canvasView.fullScreen = false;
                canvasView.canvasWidth = (float) (canvasView.getWidth());
                canvasView.canvasHeight = (float) (canvasView.getHeight());
                canvasView.canvasRatio = (canvasView.canvasWidth / canvasView.canvasHeight);
                canvasView.imageRatio = ((float) Image.getWidth() / (float) Image.getHeight());

                selectedImage = Image;
                // torch script
                Detect();

            } catch (Exception e) {
                e.printStackTrace();
                listBBox = new ArrayList<>();
            }

        } else {
            Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    private void SetLocationGuideLine(Bitmap Image) {
        //Cần có: imageWidth; imageHeight;
        // ViewWidth; ViewHeight;
        //ScreenWidth; ScreenHeight;
        int imageWidth, imageHeight, viewWidth, viewHeight, screenWidth, screenHeight;
        float expectedViewHeight;
        float max = 0.7f;
        float imageRatio = (float) Image.getWidth() / (float) Image.getHeight();
        float viewRatio = (float) imageView.getWidth() / (float) imageView.getHeight();
        float screenRatio = (float) Resources.getSystem().getDisplayMetrics().widthPixels / (float) Resources.getSystem().getDisplayMetrics().heightPixels;
        if (imageRatio > viewRatio) {
            expectedViewHeight = (float) imageView.getWidth() * (1f / imageRatio);
            float percent = expectedViewHeight / (float) Resources.getSystem().getDisplayMetrics().heightPixels;
            line.setGuidelinePercent(percent);
        } else if (imageRatio < viewRatio) {
            expectedViewHeight = (float) imageView.getWidth() * (1f / imageRatio);
            float percent = expectedViewHeight / (float) Resources.getSystem().getDisplayMetrics().heightPixels;
            if (percent > max) {
                line.setGuidelinePercent(max);
            } else {
                line.setGuidelinePercent(percent);
            }
        }
    }

    Handler handler = new Handler();

    Thread threadLocal = new Thread(new Runnable() {
        @Override
        public void run() {
            handler.post(runnableDetectLocal);
        }
    });

    Runnable runnableDetectLocal = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            ModuleResult result = utils.RunModel(model, selectedImage, 0.5f);
            canvasView.listBBox = result.listBBox;
            canvasView.invalidate();
            lsTrafficSign = utils.GetListTrafficSignForListView(result.listBBox);
            lsTrafficSign.remove(null);
            if (lsTrafficSign != null) {
                if (lsTrafficSign.isEmpty()) {
                    lsTrafficSign.add(new TrafficSign("Không tìm thấy biển báo", "info_alert", "", "Không tìm thấy biển báo trong hình", ""));
                }
                adapter.lsTrafficSign = lsTrafficSign;
                adapter.notifyDataSetChanged();
            }
        }
    };

    Thread threadRunOnServer = new Thread(new Runnable() {
        @Override
        public void run() {
            handler.post(runnableOnServer);
        }
    });

    Runnable runnableOnServer = new Runnable() {
        @Override
        public void run() {
            SocketDetector(type, selectedImage);
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    void Detect() {
        if (Constants.MethodDetect.equals(Constants.MethodDetect_Local)) {
            threadLocal.run();
        } else if (Constants.MethodDetect.equals(Constants.MethodDetect_Mb2_Server)) {
            type = "mb2-ssd-lite";
            threadRunOnServer.run();
        } else {
            type = "vgg16-ssd";
            threadRunOnServer.run();
        }
    }

    void ConnectSocket() {
        if (socket != null && socket.connected()) {
            socket.disconnect();
        }
        try {
            socket = IO.socket(Constants.ServerUrl);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.connect();
        //socket.emit("ping");

    }

    void SocketDetector(String method, Bitmap selectedImage) {
        Bitmap bmp = getResizedBitmap(selectedImage, 300, 300);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        byte[] byteImg = stream.toByteArray();
        //startDetect = System.currentTimeMillis();
        socket.emit(method, byteImg, Constants.ThresHold);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void DrawCanvasServer(String result) {
        listBBox.clear();
        String[] rs = result.split("\r\n");
        if (rs.length >= 2) {
            for (int i = 1; i < rs.length; i++) {
                String rectCo = rs[i].split(":")[2];
                String[] item = rectCo.split(",");
                float xmin = (float) (Float.valueOf(item[0]) / 300f);
                float ymin = (float) (Float.valueOf(item[1]) / 300f);
                float xmax = (float) (Float.valueOf(item[2]) / 300f);
                float ymax = (float) (Float.valueOf(item[3]) / 300f);
                RectF rect = new RectF(xmin, ymin, xmax, ymax);
                listBBox.add(new BBox(rs[i].split(":")[0], Float.valueOf(rs[i].split(":")[1]), rect));
            }
        }
        listBBox = Utils.NMSx00(listBBox, 0.5f);
        canvasView.listBBox = listBBox;
        canvasView.invalidate();
        lsTrafficSign = utils.GetListTrafficSignForListView(listBBox);
        lsTrafficSign.remove(null);
        if (lsTrafficSign != null) {
            if (lsTrafficSign.isEmpty()) {
                lsTrafficSign.add(new TrafficSign("Không tìm thấy biển báo", "info_alert", "", "Không tìm thấy biển báo trong hình", ""));
            }
            adapter.lsTrafficSign = lsTrafficSign;
            adapter.notifyDataSetChanged();
        }
    }

    void SocketResponseHandler() {
        socket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.lsTrafficSign.clear();
                        lsTrafficSign.add(new TrafficSign("Không thể kết nối đến Server", "alert_icon", "", "Vui lòng kiểm tra lại kết nối Internet", ""));
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.lsTrafficSign.clear();
                        lsTrafficSign.add(new TrafficSign("Không thể kết nối đến Server", "alert_icon", "", "Vui lòng kiểm tra lại kết nối Internet", ""));
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.lsTrafficSign.clear();
                        lsTrafficSign.add(new TrafficSign("Vui lòng chọn hình ảnh", "success_icon", "", "Vui lòng chọn hình ảnh có chứa biển báo!", ""));
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        socket.on("response", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                runOnUiThread(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void run() {
                        //responseDetect = System.currentTimeMillis();
                        //long timeExecute = responseDetect - startDetect;
                        float timePredict = Float.valueOf(args[0].toString().split("\r\n")[0].split(":")[1]) * 1000f;
                        //float timeDelay = (float) timeExecute - timePredict;
                        String result = (String) args[0];
                        Log.d("tnosTS", result);
                        DrawCanvasServer(result);
                        //txtv.setText(String.format("Time: %fms\nFPS: %f\nDelay: %fms\n-------------\n%s", (float) timeExecute, (float) (1000f / (float) timeExecute), timeDelay, result));
                    }
                });
            }
        });
    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void selectImage() {
        final CharSequence[] options = {"Chụp ảnh", "Chọn từ thư viện", "Hủy"};
        AlertDialog.Builder builder = new AlertDialog.Builder(PhotoDetector.this);
        builder.setTitle("Chọn ảnh!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Chụp ảnh")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(PhotoDetector.this, "com.example.android.fileprovider", photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(takePictureIntent, REQUEST_CODE_CAMERA);
                        }
                    }
                } else if (options[item].equals("Chọn từ thư viện")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, REQUEST_CODE_GALLERY);
                } else if (options[item].equals("Hủy")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (socket != null && socket.connected()) {
            socket.off();
            socket.disconnect();
        }
        adapter.lsTrafficSign.clear();
        adapter.notifyDataSetChanged();
        canvasView.listBBox.clear();
        canvasView.invalidate();
        handler.removeCallbacksAndMessages(null);
        this.finish();
    }
}