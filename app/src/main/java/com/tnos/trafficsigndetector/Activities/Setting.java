package com.tnos.trafficsigndetector.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.tnos.trafficsigndetector.Common.Constants;
import com.tnos.trafficsigndetector.Common.SettingsDb;
import com.tnos.trafficsigndetector.Common.Utils;
import com.tnos.trafficsigndetector.R;

import java.io.InputStream;
import java.util.Locale;

public class Setting extends AppCompatActivity {

    Button btnOK, btnCancel;
    EditText edtServer, edtThreshold;
    RadioGroup rdgMethod;
    RadioButton rdbtnMb2Local, rdbtnMb2Server, rdbtnVggServer;
    RadioButton rdbtnAudioNone, rdbtnAudioName, rdbtnAudioContent;
    Switch swDebug;
    TextView txtThres;
    SQLiteDatabase AppSettingsDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        InitViews();
        InitEvents();
        LoadSetting();

    }

    private TextToSpeech textToSpeech;

    private void InitSpeech() {
        Locale language = Locale.getDefault();
        if (language == null) {
            Toast.makeText(this, "Not language selected", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, language.getDisplayLanguage(), Toast.LENGTH_SHORT).show();
//        int result = textToSpeech.setLanguage(language);
//        if (result == TextToSpeech.LANG_MISSING_DATA) {
//            Toast.makeText(this, "Missing language data", Toast.LENGTH_SHORT).show();
//            return;
//        } else if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
//            Toast.makeText(this, "Language not supported", Toast.LENGTH_SHORT).show();
//            return;
//        } else {
//            Locale currentLanguage = textToSpeech.getVoice().getLocale();
//            Toast.makeText(this, "Language " + currentLanguage, Toast.LENGTH_SHORT).show();
//        }
    }


    private void LoadSetting() {
        edtServer.setText(Constants.ServerUrl);
        if (Constants.Debug.equals("1")) {
            swDebug.setChecked(true);
        } else {
            swDebug.setChecked(false);
        }

        if (Constants.MethodDetect.equals(Constants.MethodDetect_Local)) {
            rdbtnMb2Local.setChecked(true);
        } else if (Constants.MethodDetect.equals(Constants.MethodDetect_Mb2_Server)) {
            rdbtnMb2Server.setChecked(true);
        } else if (Constants.MethodDetect.equals(Constants.MethodDetect_VGG_Server)) {
            rdbtnVggServer.setChecked(true);
        }

        if (Constants.Sign_Audio.equals(Constants.Sign_Audio_None)) {
            rdbtnAudioNone.setChecked(true);
        } else if (Constants.Sign_Audio.equals(Constants.Sign_Audio_Content)) {
            rdbtnAudioContent.setChecked(true);
        } else if (Constants.Sign_Audio.equals(Constants.Sign_Audio_Name)) {
            rdbtnAudioName.setChecked(true);
        }

        edtThreshold.setText(String.valueOf(Constants.ThresHold));
        AppSettingsDb = SettingsDb.initDatabase(this, Constants.DatabaseName);
    }

    private void InitViews() {
        btnOK = (Button) this.findViewById(R.id.set_btn_ok);
        btnCancel = (Button) this.findViewById(R.id.set_btn_cancel);
        edtServer = (EditText) this.findViewById(R.id.set_inp_server);
        edtThreshold = (EditText) this.findViewById(R.id.set_inp_threshold);
        rdgMethod = (RadioGroup) this.findViewById(R.id.set_grp_method);
        rdbtnMb2Local = (RadioButton) this.findViewById(R.id.set_rdbtn_mb2local);
        rdbtnMb2Server = (RadioButton) this.findViewById(R.id.set_rdbtn_mb2server);
        rdbtnVggServer = (RadioButton) this.findViewById(R.id.set_rdbtn_vggserver);
        rdbtnAudioNone = (RadioButton) this.findViewById(R.id.set_rdbtn_audio_none);
        rdbtnAudioName = (RadioButton) this.findViewById(R.id.set_rdbtn_audio_name);
        rdbtnAudioContent = (RadioButton) this.findViewById(R.id.set_rdbtn_audio_content);
        swDebug = (Switch) this.findViewById(R.id.set_sw_debug);
        txtThres = (TextView) this.findViewById(R.id.set_txt_thres);
    }

    private void InitEvents() {

        edtThreshold.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (Float.valueOf(edtThreshold.getText().toString()) > 1f) {
                        edtThreshold.setText("1");
                    }
                } catch (Exception e) {

                }
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenDialog(Float.valueOf(edtThreshold.getText().toString()));
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Setting.this.onBackPressed();
            }
        });
    }

    private void OpenDialog(float thres) {
        if (thres >= 0.8f) {
            SaveChanges();
            Setting.super.onBackPressed();
        } else {
            String msg = "";
            if (thres < 0.35f) {
                msg = "Ngưỡng nhận diện quá thấp, ứng dụng sẽ phát hiện được nhiều biển báo nhưng độ tin cậy sẽ không cao!";
            }
            if (0.35f <= thres && thres <= 0.5f) {
                msg = "Ngưỡng nhận diện thấp, ứng dụng sẽ hoạt động với độ tin cậy không cao!";
            }
            if (thres >= 0.5f && thres < 0.8f) {
                msg = "Ngưỡng nhận diện nằm trong mức trung bình, ứng dụng sẽ hoạt động với độ tin cậy không cao!";
            }

            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_traffic_sign_detail);
            TextView txtTitle, txtSignContent, txtSignDescription;
            ImageView imgSign;
            Button btnOK, btnCancel;
            //InitViews
            txtTitle = (TextView) dialog.findViewById(R.id.dialog_toolbar_title);
            txtSignContent = (TextView) dialog.findViewById(R.id.dialog_txt_sign_name);
            txtSignDescription = (TextView) dialog.findViewById(R.id.dialog_txt_description);
            txtSignDescription.setMovementMethod(new ScrollingMovementMethod());
            imgSign = (ImageView) dialog.findViewById(R.id.dialog_img_sign);
            btnOK = (Button) dialog.findViewById(R.id.dialog_btn_ok);
            btnCancel = (Button) dialog.findViewById(R.id.dialog_btn_cancel);
            btnCancel.setVisibility(View.VISIBLE);
            //SetValue
            txtTitle.setText("CẢNH BÁO");
            txtSignContent.setText("Ngưỡng phát hiện: " + thres);
            txtSignDescription.setText(msg);
            String imgPath = Utils.assetFilePath(this, "alert_icon.PNG");
            Bitmap bmp = loadBitmapFromAssets(this, imgPath);
            imgSign.setImageBitmap(bmp);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SaveChanges();
                    dialog.dismiss();
                    Setting.super.onBackPressed();
                }
            });
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    ;

    public Bitmap loadBitmapFromAssets(Context context, String path) {
        InputStream stream = null;
        try {
            return BitmapFactory.decodeFile(path);
        } catch (Exception ignored) {
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    private void SaveChanges() {
        Constants.Debug = swDebug.isChecked() ? "1" : "0";
        Constants.ServerUrl = edtServer.getText().toString();
        try {
            Constants.ThresHold = Float.valueOf(edtThreshold.getText().toString());
        } catch (Exception ex) {
            Constants.ThresHold = 0.35f;
        }
        Constants.MethodDetect = rdbtnMb2Local.isChecked() ? "1" : rdbtnMb2Server.isChecked() ? "2" : rdbtnVggServer.isChecked() ? "3" : "3";
        Constants.Sign_Audio = rdbtnAudioNone.isChecked() ? Constants.Sign_Audio_None : rdbtnAudioName.isChecked() ? Constants.Sign_Audio_Name : rdbtnAudioContent.isChecked() ? Constants.Sign_Audio_Content : Constants.Sign_Audio_Content;

        ContentValues contentValues = new ContentValues();
        contentValues.put("Value", Constants.MethodDetect);
        AppSettingsDb.update("AppValue", contentValues, "Key=?", new String[]{"Method"});
        contentValues.clear();

        contentValues.put("Value", Constants.ServerUrl);
        AppSettingsDb.update("AppValue", contentValues, "Key=?", new String[]{"Uri"});

        contentValues.put("Value", String.valueOf(Constants.ThresHold));
        AppSettingsDb.update("AppValue", contentValues, "Key=?", new String[]{"Threshold"});

        contentValues.put("Value", Constants.Debug);
        AppSettingsDb.update("AppValue", contentValues, "Key=?", new String[]{"Debug"});

        contentValues.put("Value", Constants.Sign_Audio);
        AppSettingsDb.update("AppValue", contentValues, "Key=?", new String[]{"Audio"});


        Toast.makeText(Setting.this, "Cài đặt thành công!", Toast.LENGTH_LONG).show();

    }
}