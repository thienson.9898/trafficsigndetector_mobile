package com.tnos.trafficsigndetector.Activities.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tnos.trafficsigndetector.Common.Constants;
import com.tnos.trafficsigndetector.Common.Utils;
import com.tnos.trafficsigndetector.Entities.TrafficSign;
import com.tnos.trafficsigndetector.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ListTrafficSignAdapter extends BaseAdapter {
    public ListTrafficSignAdapter(Activity myContext, ArrayList<TrafficSign> lsTrafficSign, int myLayout) {
        this.myContext = myContext;
        this.lsTrafficSign = lsTrafficSign;
        this.myLayout = myLayout;
    }

    Activity myContext;
    public ArrayList<TrafficSign> lsTrafficSign;
    int myLayout;

    @Override
    public int getCount() {
        return lsTrafficSign.size();
    }

    @Override
    public Object getItem(int position) {
        return lsTrafficSign.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private int width = Resources.getSystem().getDisplayMetrics().widthPixels;
    private int height = Resources.getSystem().getDisplayMetrics().heightPixels;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_list_view_traffic_sign, null);
        TextView txtName, txtContent;
        ImageView img;

        TrafficSign trafficSign = lsTrafficSign.get(position);
        txtName = (TextView) convertView.findViewById(R.id.item_LsV_TS_sign_name);
        txtContent = (TextView) convertView.findViewById(R.id.item_LsV_TS_sign_content);
        img = (ImageView) convertView.findViewById(R.id.item_LsV_TS_image);
        String imgPath = "";
        try {
            imgPath = Utils.assetFilePath(myContext, Constants.TrafficSignFolder, trafficSign.drawableSource + ".PNG");
        }
        catch (Exception ex){
            imgPath = "";
        }
        //        String imgPath = Utils.assetFilePath(myContext, trafficSign.drawableSource + ".PNG");
        Bitmap bmp = loadBitmapFromAssets(myContext, imgPath);
        img.setImageBitmap(bmp);

        txtName.setText(trafficSign.name);
        txtContent.setText(trafficSign.content);

        if(!(trafficSign.type.equals("")||trafficSign.description.equals(""))){
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(myContext);
                    dialog.setContentView(R.layout.dialog_traffic_sign_detail);
                    TextView txtTitle, txtSignContent, txtSignDescription;
                    ImageView imgSign;
                    Button btnOK;
                    //InitViews
                    txtTitle = (TextView) dialog.findViewById(R.id.dialog_toolbar_title);
                    txtSignContent = (TextView) dialog.findViewById(R.id.dialog_txt_sign_name);
                    txtSignDescription = (TextView) dialog.findViewById(R.id.dialog_txt_description);
                    txtSignDescription.setMovementMethod(new ScrollingMovementMethod());
                    imgSign = (ImageView) dialog.findViewById(R.id.dialog_img_sign);
                    btnOK = (Button) dialog.findViewById(R.id.dialog_btn_ok);
                    //SetValue
                    txtTitle.setText("Biển báo " + trafficSign.name);
                    txtSignContent.setText("Biển báo " + trafficSign.content);
                    txtSignDescription.setText("Nhóm biển báo: " + trafficSign.type.toLowerCase().replace("nhóm ", "") + "\n" + trafficSign.description);
                    String imgPath = Utils.assetFilePath(myContext, Constants.TrafficSignFolder, trafficSign.drawableSource + ".PNG");
                    Bitmap bmp = loadBitmapFromAssets(myContext, imgPath);
                    imgSign.setImageBitmap(bmp);
                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
        }

        return convertView;
    }

    public Bitmap loadBitmapFromAssets(Context context, String path) {
        InputStream stream = null;
        try {
            return BitmapFactory.decodeFile(path);
        } catch (Exception ignored) {
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception ignored) {
            }
        }
        return null;
    }
}