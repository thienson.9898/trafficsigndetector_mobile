package com.tnos.trafficsigndetector;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tnos.trafficsigndetector.Activities.PhotoDetector;
import com.tnos.trafficsigndetector.Activities.RealTimeDetector;
import com.tnos.trafficsigndetector.Activities.Setting;
import com.tnos.trafficsigndetector.Common.CameraPreview;
import com.tnos.trafficsigndetector.Common.Constants;
import com.tnos.trafficsigndetector.Common.SettingsDb;
import com.tnos.trafficsigndetector.Common.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Permission;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase AppSettingsDb;
    LinearLayout btnPhoto, btnRealtime;
    ImageView btnPhotoImg, btnRealtimeImg;
    TextView btnPhotoTxt, btnRealtimeTxt;
    ImageButton btnSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        App_Start();
        InitView();
        InitEvent();
        RequirePermission();
    }

    private void App_Start() {
        Constants.TrafficSignsInfo = Utils.loadJSONFromAsset(MainActivity.this, "TrafficSigns.Json");
        Utils.JsonToTrafficSignObject();
        AppSettingsDb = SettingsDb.initDatabase(this, Constants.DatabaseName);
        Cursor cs = AppSettingsDb.rawQuery("Select * from AppValue", null);
        for (int i = 0; i < cs.getCount(); i++) {
            cs.moveToPosition(i);
            if (cs.getString(0).equals("Uri")) {
                Constants.ServerUrl = cs.getString(1);
            }
            if (cs.getString(0).equals("Method")) {
                Constants.MethodDetect = cs.getString(1);
            }
            if (cs.getString(0).equals("Threshold")) {
                Constants.ThresHold = Float.valueOf(cs.getString(1));
            }
            if (cs.getString(0).equals("Debug")) {
                Constants.Debug = cs.getString(1);
            }
            if (cs.getString(0).equals("Audio")) {
                Constants.Sign_Audio = cs.getString(1);
            }
            Log.d("tnosTS", cs.getString(1));
        }
    }

    private void RequirePermission() {
        String[] PERMISSIONS = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET
        };
        ActivityCompat.requestPermissions(
                MainActivity.this,
                PERMISSIONS,
                45
        );

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean GrantPermission = false;

        if (CheckPermission(Manifest.permission.INTERNET)
                && CheckPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                && CheckPermission(Manifest.permission.CAMERA)
                && CheckPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            GrantPermission = true;
        }
        if (!GrantPermission) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Bạn chưa cấp quyền bộ nhớ và Camera cho ứng dụng, Ứng dụng sẽ không thể hoạt động!")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setPositiveButton("Cấp lại quyền", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            RequirePermission();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void InitView() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        btnPhoto = (LinearLayout) this.findViewById(R.id.main_btn_photo_detect);
        btnRealtime = (LinearLayout) this.findViewById(R.id.main_btn_realtime_detect);
        btnPhotoImg = (ImageView) this.findViewById(R.id.main_btn_photo_detect_img);
        btnRealtimeImg = (ImageView) this.findViewById(R.id.main_btn_realtime_detect_img);
        btnPhotoTxt = (TextView) this.findViewById(R.id.main_btn_photo_detect_txtv);
        btnRealtimeTxt = (TextView) this.findViewById(R.id.main_btn_realtime_detect_txtv);
        btnSetting = (ImageButton) this.findViewById(R.id.main_btn_setting);
    }

    void InitEvent() {
        btnPhoto.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int orientation = MainActivity.this.getResources().getConfiguration().orientation;
                if(orientation == Configuration.ORIENTATION_PORTRAIT) {
                    ToggleViewBackground(btnPhoto, event, getDrawable(R.drawable.bg_main_rounded_corner_top));
                }
                else{
                    ToggleViewBackground(btnPhoto, event, getDrawable(R.drawable.bg_main_rounded_corner_left));
                }
                return false;
            }
        });
        btnRealtime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int orientation = MainActivity.this.getResources().getConfiguration().orientation;
                if(orientation == Configuration.ORIENTATION_PORTRAIT) {
                    ToggleViewBackground(btnRealtime, event, getDrawable(R.drawable.bg_main_rounded_corner_bottom));
                }
                else{
                    ToggleViewBackground(btnRealtime, event, getDrawable(R.drawable.bg_main_rounded_corner_right));
                }
                return false;
            }
        });
        btnPhotoImg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int orientation = MainActivity.this.getResources().getConfiguration().orientation;
                if(orientation == Configuration.ORIENTATION_PORTRAIT) {
                    ToggleViewBackground(btnPhoto, event, getDrawable(R.drawable.bg_main_rounded_corner_top));
                }
                else{
                    ToggleViewBackground(btnPhoto, event, getDrawable(R.drawable.bg_main_rounded_corner_left));
                }
                return false;
            }
        });
        btnRealtimeImg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int orientation = MainActivity.this.getResources().getConfiguration().orientation;
                if(orientation == Configuration.ORIENTATION_PORTRAIT) {
                    ToggleViewBackground(btnRealtime, event, getDrawable(R.drawable.bg_main_rounded_corner_bottom));
                }
                else{
                    ToggleViewBackground(btnRealtime, event, getDrawable(R.drawable.bg_main_rounded_corner_right));
                }
                return false;
            }
        });
        btnPhotoTxt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int orientation = MainActivity.this.getResources().getConfiguration().orientation;
                if(orientation == Configuration.ORIENTATION_PORTRAIT) {
                    ToggleViewBackground(btnPhoto, event, getDrawable(R.drawable.bg_main_rounded_corner_top));
                }
                else{
                    ToggleViewBackground(btnPhoto, event, getDrawable(R.drawable.bg_main_rounded_corner_left));
                }
                return false;
            }
        });
        btnRealtimeTxt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int orientation = MainActivity.this.getResources().getConfiguration().orientation;
                if(orientation == Configuration.ORIENTATION_PORTRAIT) {
                    ToggleViewBackground(btnRealtime, event, getDrawable(R.drawable.bg_main_rounded_corner_bottom));
                }
                else{
                    ToggleViewBackground(btnRealtime, event, getDrawable(R.drawable.bg_main_rounded_corner_right));
                }
                return false;
            }
        });
        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToPhotoDetect();
            }
        });
        btnPhotoImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToPhotoDetect();
            }
        });

        btnRealtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToRealTimeDetect();
            }
        });
        btnRealtimeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToRealTimeDetect();
            }
        });

        btnRealtimeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToRealTimeDetect();
            }
        });
        btnPhotoTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoToPhotoDetect();
            }
        });
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Setting.class);
                startActivity(intent);
            }
        });
    }

    private void ToggleViewBackground(View view, MotionEvent event, Drawable drawable) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            view.setBackground(null);
        } else {
            view.setBackground(drawable);
        }
    }

    void GoToPhotoDetect() {
        Intent intent = new Intent(MainActivity.this, PhotoDetector.class);
        startActivity(intent);
    }

    void GoToRealTimeDetect() {
        Intent intent = new Intent(MainActivity.this, RealTimeDetector.class);
        startActivity(intent);
    }

    private boolean CheckPermission(String permission) {
        int result = PermissionChecker.checkSelfPermission(getApplicationContext(), permission);
        if (result == PermissionChecker.PERMISSION_GRANTED) {
            return true;
        } else
            return false;
    }
}

