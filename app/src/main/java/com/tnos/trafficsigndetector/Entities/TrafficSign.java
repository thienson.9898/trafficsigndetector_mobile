package com.tnos.trafficsigndetector.Entities;

public class TrafficSign{
    public String name;
    public String drawableSource;
    public String description;
    public String content;
    public String type;

    public TrafficSign() {
    }

    public TrafficSign(String name, String drawableSource, String description, String content, String type) {
        this.name = name;
        this.drawableSource = drawableSource;
        this.description = description;
        this.content = content;
        this.type = type;
    }
}