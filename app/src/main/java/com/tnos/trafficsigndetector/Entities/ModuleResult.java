package com.tnos.trafficsigndetector.Entities;

import java.util.ArrayList;
import java.util.List;

public class ModuleResult {
    public String result;
    public long timePredict;
    public long timeExecute;
    public List<BBox> listBBox = new ArrayList<>();
}
