package com.tnos.trafficsigndetector.Entities;

import android.content.Context;

import com.tnos.trafficsigndetector.Common.Utils;

import org.pytorch.Module;

import java.io.File;

public class TorchModel {
    public TorchModel(Context ctx) {
        this.ctx = ctx;
        this.moduleFileAbsoluteFilePath = new File(pathModel).getAbsolutePath();
        this.model = Module.load(this.moduleFileAbsoluteFilePath);
    }

    public Context ctx;
    public static String[] classes = {"BACKGROUND", "500", "300", "P.123b", "S.505a", "P.128", "R.415", "P.115", "P.110", "W.208", "R.303", "W.225", "P.112", "P.104", "P.107", "P.103a", "R.411", "W.245", "P.123a", "I.409", "W.205", "P.124a", "I.424", "S.508", "I.414", "R.302a", "R.403", "P.117", "S.509", "P.127", "W.207", "R.412", "200", "400", "100", "P.102", "P.130", "P.131a", "I.423"};
    String pathModel = Utils.assetFilePath(ctx, "gg.pth");
    final String moduleFileAbsoluteFilePath;
    public Module model;
}
