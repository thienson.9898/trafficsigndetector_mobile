package com.tnos.trafficsigndetector.Entities;

import android.graphics.RectF;

public class BBox {
    public BBox(String label, Float confidence, RectF rectF) {
        this.label = label;
        this.confidence = confidence;
        this.rectF = rectF;
    }

    String label;

    public String getLabel() {
        return label;
    }

    public Float getConfidence() {
        return confidence;
    }

    public RectF getRectF() {
        return rectF;
    }

    Float confidence;
    RectF rectF;
}
