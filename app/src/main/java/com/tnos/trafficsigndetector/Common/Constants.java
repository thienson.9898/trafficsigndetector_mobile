package com.tnos.trafficsigndetector.Common;

import android.content.Context;

import com.github.nkzawa.socketio.client.Url;
import com.tnos.trafficsigndetector.Activities.PhotoDetector;
import com.tnos.trafficsigndetector.Entities.TrafficSign;

import java.util.ArrayList;
import java.util.List;

public class Constants {
    public static String TrafficSignsInfo;
    public static final String DatabaseName = "AppSettings.db";

    public static String ServerUrl;
    public static String MethodDetect = "1";
    public static String Debug = "0";

    public static String MethodDetect_Local = "1";
    public static String MethodDetect_Mb2_Server = "2";
    public static String MethodDetect_VGG_Server = "3";

    public static String TrafficSignFolder = "traffic_signs";
    public static String SoundsFolder = "sounds";

    public static Float ThresHold = 0.2f;

    public static String Sign_Audio = "Content";
    public static String Sign_Audio_None = "None";
    public static String Sign_Audio_Name = "Name";
    public static String Sign_Audio_Content = "Content";

    //Khoảng cách thời gian để xóa list biển
    public static long timeDiff = 10000;

    public static ArrayList<TrafficSign> listTrafficSign;
}
