package com.tnos.trafficsigndetector.Common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Build;
import android.os.Handler;
import android.util.JsonReader;
import android.util.Log;
import android.view.PixelCopy;
import android.view.SurfaceView;

import com.tnos.trafficsigndetector.Entities.BBox;
import com.tnos.trafficsigndetector.Entities.ModuleResult;
import com.tnos.trafficsigndetector.Entities.TorchModel;
import com.tnos.trafficsigndetector.Entities.TrafficSign;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pytorch.IValue;
import org.pytorch.Module;
import org.pytorch.Tensor;
import org.pytorch.torchvision.TensorImageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;

public class Utils {

    public static String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public static RectF convertToBoxes(RectF rect, Float paddingTop, Float paddingLeft, Float imageWidth, Float imageHeight) {
        Float left = ((rect.left * imageWidth) + paddingLeft);
        Float top = ((rect.top * imageHeight) + paddingTop);

        Float right = ((rect.right * imageWidth) + paddingLeft);
        Float bottom = ((rect.bottom * imageHeight) + paddingTop);
        return new RectF(left, top, right, bottom);
    }

    public static String assetFilePath(Context context, String subDir, String assetName) {
        assetName = (subDir.equals(null) || subDir.equals("")) ? assetName : subDir + File.separator + assetName;
        File file = new File(context.getFilesDir(), assetName);
        if (!(subDir.equals(null) || subDir.equals(""))) {
            File dir = new File(context.getFilesDir(), subDir);
            if (!dir.exists()) {
                dir.mkdir();
            }
        }
        if (file.exists() && file.length() > 0) {
            return file.getAbsolutePath();
        }
        Log.d("tnosTS", file.getAbsolutePath());
        try (InputStream is = context.getAssets().open(assetName)) {
            try (OutputStream os = new FileOutputStream(file)) {
                byte[] buffer = new byte[4 * 1024];
                int read;
                while ((read = is.read(buffer)) != -1) {
                    os.write(buffer, 0, read);
                }
                os.flush();
            }
            return file.getAbsolutePath();
        } catch (IOException e) {
            Log.e("Constants.TAG", "Error process asset " + assetName + " to file path");
        }
        return null;
    }

    public static String assetFilePath(Context context, String assetName) {
        return assetFilePath(context, "", assetName);
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        //bm.recycle();
        return resizedBitmap;
    }

    public static List<BBox> NMS(List<BBox> lsBBox, float threshold, boolean NMSx00) {
        if (lsBBox == null || lsBBox.isEmpty()) {
            return new ArrayList<>();
        }
        for (int i = 0; i < lsBBox.size(); i++) {
            if (lsBBox.get(i) != null) {
                for (int j = i + 1; j < lsBBox.size(); j++) {
                    if (lsBBox.get(i) != null && lsBBox.get(j) != null) {
                        if (lsBBox.get(i).getLabel() == lsBBox.get(j).getLabel()) {
                            if (IOU(lsBBox.get(i).getRectF(), lsBBox.get(j).getRectF()) > threshold) {
                                if (lsBBox.get(i).getConfidence() >= lsBBox.get(j).getConfidence()) {
                                    lsBBox.set(j, null);
                                } else {
                                    lsBBox.set(i, null);
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (NMSx00)
            return NMSx00(lsBBox, threshold);
        return lsBBox;
    }

    public static List<BBox> NMS(List<BBox> lsBBox, float threshold) {
        if (lsBBox == null || lsBBox.isEmpty()) {
            return new ArrayList<>();
        }
        for (int i = 0; i < lsBBox.size(); i++) {
            if (lsBBox.get(i) != null) {
                for (int j = i + 1; j < lsBBox.size(); j++) {
                    if (lsBBox.get(i) != null && lsBBox.get(j) != null) {
                        if (lsBBox.get(i).getLabel() == lsBBox.get(j).getLabel()) {
                            if (IOU(lsBBox.get(i).getRectF(), lsBBox.get(j).getRectF()) > threshold) {
                                if (lsBBox.get(i).getConfidence() >= lsBBox.get(j).getConfidence()) {
                                    lsBBox.set(j, null);
                                } else {
                                    lsBBox.set(i, null);
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }
        return lsBBox;
    }

    public static List<BBox> NMSx00(List<BBox> lsBBox, float threshold) {
        if (lsBBox == null || lsBBox.isEmpty()) {
            return new ArrayList<>();
        }
        for (int i = 0; i < lsBBox.size(); i++) {
            if (lsBBox.get(i) != null) {
                for (int j = i + 1; j < lsBBox.size(); j++) {
                    if (lsBBox.get(i) != null && lsBBox.get(j) != null) {
                        if (lsBBox.get(j).getLabel().contains("00")) {
                            if (IOU(lsBBox.get(i).getRectF(), lsBBox.get(j).getRectF()) > threshold) {
                                lsBBox.set(j, null);
                            }
                        }
                    }
                }
            }
        }
        return lsBBox;
    }

    public static float IOU(RectF a, RectF b) {
        float imgSize = 300;
        if (a.intersect(b)) {
            PointF l1 = new PointF(a.left * imgSize, a.top * imgSize);
            PointF r1 = new PointF(a.right * imgSize, a.bottom * imgSize);

            PointF l2 = new PointF(b.left * imgSize, b.top * imgSize);
            PointF r2 = new PointF(b.right * imgSize, b.bottom * imgSize);

            float overlap = (Math.abs(l1.x - r1.x) * Math.abs(l1.y - r1.y)) + (Math.abs(l2.x - r2.x) * Math.abs(l2.y - r2.y));
            float intersect = (Math.min(r1.x, r2.x) - Math.max(l1.x, l2.x)) * (Math.min(r1.y, r2.y) - Math.max(l1.y, l2.y));
            overlap -= intersect;
            return intersect / overlap;
        }
        return 0f;
    }

    public static void JsonToTrafficSignObject() {
        try {
            if (Constants.listTrafficSign == null) {
                Constants.listTrafficSign = new ArrayList<>();
            }
            JSONArray jsonArray = new JSONArray(Constants.TrafficSignsInfo);
            for (int i = 0; i < jsonArray.length(); i++) {
                TrafficSign ts = new TrafficSign();
                JSONObject obj = jsonArray.getJSONObject(i);
                ts.name = obj.getString("Name");
                ts.description = obj.getString("Description");
                ts.content = obj.getString("Content");
                ts.type = obj.getString("Type");
                ts.drawableSource = obj.getJSONArray("ListImgName").getString(0);
                Constants.listTrafficSign.add(ts);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Bitmap getBitmapFromView(SurfaceView view) {
        try {
            final Handler handlerThread = new Handler();
            final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
            PixelCopy.OnPixelCopyFinishedListener listener = new PixelCopy.OnPixelCopyFinishedListener() {
                @Override
                public void onPixelCopyFinished(int copyResult) {

                }
            };
            PixelCopy.request(view, bitmap, listener, handlerThread);
//        saveImage(bitmap);
            return bitmap;
        } catch (Exception ex) {
            Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
            Bitmap bmp = Bitmap.createBitmap(300, 300, conf);
            return bmp;
        }
    }

    ModuleResult moduleResult = new ModuleResult();
    String[] classes = TorchModel.classes;

    public ModuleResult RunModel(Module model, Bitmap img, Float NMSThreshold) {
        moduleResult.listBBox.clear();
        long startExecute = System.currentTimeMillis();
        Bitmap resizedBitmap = img;
        if (!(img.getWidth() == 300 && img.getHeight() == 300)) {
            resizedBitmap = getResizedBitmap(img, 300, 300);
        }
        Tensor inputTensor = TensorImageUtils.bitmapToFloat32Tensor(resizedBitmap,
                TensorImageUtils.TORCHVISION_NORM_MEAN_RGB, TensorImageUtils.TORCHVISION_NORM_STD_RGB);
        long startPredict = System.currentTimeMillis();
        final IValue output = model.forward(IValue.from(inputTensor));
        long endPredict = System.currentTimeMillis();
        IValue[] outputTuple = output.toTuple();
        String txtResult = "";
        //Hien thi kq
        float[] scores = outputTuple[0].toTensor().getDataAsFloatArray();
        float[] locations = outputTuple[1].toTensor().getDataAsFloatArray();
        //int[] idx; //Luu id toa do ....v.v.v..
        String text = "";
        for (int i = 0; i < scores.length; i++) {
            if (scores[i] > Constants.ThresHold) {
                int idx = i / 39;
                if (i - 39 * idx != 0) {
                    moduleResult.listBBox.add(new BBox(classes[i - 39 * idx], scores[i], new RectF(locations[idx * 4], locations[(idx * 4) + 1], locations[(idx * 4) + 2], locations[(idx * 4) + 3])));
                }
            }
        }
        moduleResult.listBBox = Utils.NMS(moduleResult.listBBox, NMSThreshold, true);
        text = "";
        if (moduleResult.listBBox != null && !moduleResult.listBBox.isEmpty())
            for (int i = 0; i < moduleResult.listBBox.size(); i++) {
                if (moduleResult.listBBox.get(i) != null)
                    text += "\n" + String.format("%s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f", moduleResult.listBBox.get(i).getLabel(), moduleResult.listBBox.get(i).getConfidence(), moduleResult.listBBox.get(i).getRectF().left, moduleResult.listBBox.get(i).getRectF().top, moduleResult.listBBox.get(i).getRectF().right, moduleResult.listBBox.get(i).getRectF().bottom);
            }
        long endExecute = System.currentTimeMillis();
        long timePredict = endPredict - startPredict;
        long timeExecute = endExecute - startExecute;
        moduleResult.result = text;
        moduleResult.timePredict = timePredict;
        moduleResult.timeExecute = timeExecute;
        return moduleResult;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public ArrayList<TrafficSign> GetListTrafficSignForListView(List<BBox> listBBox) {
        ArrayList<TrafficSign> lsResult = new ArrayList<>();
        for (int i = 0; i < listBBox.size(); i++) {
            if (listBBox.get(i) != null) {
                final String label = listBBox.get(i).getLabel();
                if (!DoTrafficSignContain(lsResult, label)) {
                    lsResult.add(GetTrafficSignByName(label));
                }
            }
        }
        return lsResult;
    }

    private boolean DoTrafficSignContain(List<TrafficSign> listTrafficSign, String trafficSignName) {
        if (listTrafficSign == null || listTrafficSign.isEmpty()) {
            return false;
        }
        for (int i = 0; i < listTrafficSign.size(); i++) {
            if (listTrafficSign.get(i) != null) {
                if (listTrafficSign.get(i).name.equals(trafficSignName)) {
                    return true;
                }
            }
        }
        return false;
    }

    private TrafficSign GetTrafficSignByName(String name) {
        for (int i = 0; i < Constants.listTrafficSign.size(); i++) {
            if (Constants.listTrafficSign.get(i).name.equals(name)) {
                return Constants.listTrafficSign.get(i);
            }
        }
        return null;
    }

    public static String GetLinkAudio(Context ctx, String signName) {
        signName += ".mp3";
        String type = Constants.Sign_Audio;
        String link = "";
        if (type.equals(Constants.Sign_Audio_None)) {
            link = "";
        }
        if (type.equals(Constants.Sign_Audio_Name)) {
            link = assetFilePath(ctx, "sound_name", signName);
        }
        if (type.equals(Constants.Sign_Audio_Content)) {
            link = assetFilePath(ctx, "sound_content", signName);
        }
        return link;
    }
}
