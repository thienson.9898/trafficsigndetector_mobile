package com.tnos.trafficsigndetector.Common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.tnos.trafficsigndetector.Entities.BBox;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

public class CanvasView extends View {
    Paint mPaint;
    Paint mPen;
    public static List<BBox> listBBox = new ArrayList<>();
    public Float canvasWidth = 0f;
    public Float canvasHeight = 0f;
    private Float imageWidth = 0f;
    private Float imageHeight = 0f;
    public Float canvasRatio;
    public Float imageRatio;
    public Boolean fullScreen = true;
    Utils utils;
    private Float threshold = 0.5f;

    public CanvasView(Context context) {
        super(context);
        utils = new Utils();
        initPaint();
    }


    public CanvasView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initPaint() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.RED);
        //mPaint.setAlpha(200);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(5.0f);

        mPen = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPen.setStyle(Paint.Style.FILL);
        mPen.setColor(Color.RED);
        mPen.setStrokeWidth(1.0f);
        int spSize = 15;
        float scaledSizeInPixels = spSize * getResources().getDisplayMetrics().scaledDensity;
        mPen.setTextSize(scaledSizeInPixels);

    }

    @Override
    public void addOnAttachStateChangeListener(OnAttachStateChangeListener listener) {
        super.addOnAttachStateChangeListener(listener);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvasWidth = Float.valueOf(canvas.getWidth());
        canvasHeight = Float.valueOf(canvas.getHeight());
        if (listBBox != null && !listBBox.isEmpty()) {
            if (fullScreen) {
                for (int i = 0; i < listBBox.size(); i++) {
                    if (listBBox.get(i) != null) {
                        RectF rectF;
                        rectF = utils.convertToBoxes(listBBox.get(i).getRectF(), 0f, 0f, canvasWidth, canvasHeight);
                        canvas.drawRect(rectF, mPaint);
                        canvas.drawText(String.format("%s: %.2f", listBBox.get(i).getLabel(), listBBox.get(i).getConfidence()), rectF.left, rectF.top - 15f, mPen);
                    }
                }
            } else {
                //Canvas draw: img
                Float paddingLeft = imageRatio < canvasRatio ? ((canvasWidth - canvasHeight * imageRatio) / 2) : 0;
                Float paddingTop = imageRatio > canvasRatio ? ((canvasHeight - canvasWidth / imageRatio) / 2) : 0;
                imageWidth = paddingLeft == 0f ? canvasWidth : canvasWidth - 2 * paddingLeft;
                imageHeight = paddingTop == 0f ? canvasHeight : canvasHeight - 2 * paddingTop;
                for (int i = 0; i < listBBox.size(); i++) {
                    if (listBBox.get(i) != null) {
                        RectF rectF;
                        rectF = utils.convertToBoxes(listBBox.get(i).getRectF(), paddingTop, paddingLeft, imageWidth, imageHeight);
                        canvas.drawRect(rectF, mPaint);
                        canvas.drawText(String.format("%s: %.2f", listBBox.get(i).getLabel(), listBBox.get(i).getConfidence()), rectF.left, rectF.top - 15f, mPen);
                    }
                }
            }
        } else {
            canvas.drawColor(Color.TRANSPARENT);
        }
    }
}
